package br.com.senac.model;

public class Contato {

    private String nome;
    private String telefone;
    private String email;
    private String tipoContato;

    public Contato(String nome, String telefone, String email, String tipoContato) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.tipoContato = tipoContato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(String tipoContato) {
        this.tipoContato = tipoContato;
    }

}
