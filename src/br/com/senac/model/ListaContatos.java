package br.com.senac.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class ListaContatos {

    private static List<Contato> agenda = new ArrayList<>();
    private int numContatos ;

    public static int getNumContatos() {
        return agenda.size();
        
    }

    public void setNumContatos(int numContatos) {
        this.numContatos = numContatos;
    }

    public static void adicionar(Contato contato) {
        agenda.add(contato);
//        JOptionPane.showMessageDialog(this, "Salvo com Sucesso");
        System.out.println("Salvo com Sucesso");

    }

    public static void remover(Contato contato) {
        agenda.remove(contato);
        System.out.println("Removido com Sucesso");
    }

    public static void remover(int indice) {
        agenda.remove(indice);
    }

//    public static List<Contato> getAgenda() {
//        return agenda;
//    }
//
//    public static void setAgenda(List<Contato> agenda) {
//        ListaContatos.agenda = agenda;
//    }
}
